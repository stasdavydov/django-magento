from django.contrib import admin

# Register your models here.
from mymagento.models import Product


class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ('pk', 'name', 'sku', 'price', 'created', 'updated', 'magento_id', 'magento_created_at', 'magento_updated_at')


admin.site.register(Product, ProductAdmin)