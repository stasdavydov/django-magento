from __future__ import unicode_literals

from django.apps import AppConfig


class MyMagentoConfig(AppConfig):
    name = 'mymagento'
    verbose_name = 'My Magento'
