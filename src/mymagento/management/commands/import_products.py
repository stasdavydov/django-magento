from __future__ import unicode_literals

import pytz
import requests
from decimal import Decimal

from datetime import datetime
from django.conf import settings
from django.core.management import BaseCommand
from django.core.management.base import CommandError
from django.utils import timezone

from mymagento.management import magento
from mymagento.models import Product


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('host', type=str, help='Host IP or name')
        parser.add_argument('username', type=str, help='Magento user name')
        parser.add_argument('password', type=str, help='Magento user password')
        parser.add_argument('--updated_after', type=str, default=None, dest='updated_after',
                            help='Only products updated after the date and time')
        parser.add_argument('--force', action='store_true', default=False, dest='force', help='Force update')
        parser.add_argument('--server-timezone', type=str, default=settings.TIME_ZONE, dest='server-timezone',
                            help='Server timezone')

    def handle(self, *args, **options):
        host = options.get('host')
        print('Import Magento products from {}'.format(host))
        server_timezone = pytz.timezone(options.get('server-timezone', settings.TIME_ZONE))
        print('Use server timezone {}'.format(server_timezone))
        token = magento.auth(host, options.get('username'), options.get('password'))

        r = requests.get(magento.rest_url(host, 'products?{0}'.format(magento.search_criteria(
            (['updated_at', options['updated_after'], 'gteq'],) if options['updated_after'] else ()))),
                         headers={'Authorization': 'Bearer {0}'.format(token)})
        if r.status_code is not 200:
            raise CommandError('Cannot get product list. Status code: {0.status_code}. Response: {0.content}'.format(r))

        products = r.json()
        print('List of {0[total_count]} products is retrieved'.format(products))

        force_update = options.get('force', False)
        for product in products['items']:
            self.import_product(product, server_timezone, force=force_update)

    def import_product(self, magento_product, server_timezone, force=False):
        try:
            product = Product.objects.get(magento_id=magento_product['id'])
        except Product.DoesNotExist:
            product = None

        magento_product_created = timezone.make_aware(datetime.strptime(magento_product['created_at'], '%Y-%m-%d %H:%M:%S'), timezone=server_timezone)
        magento_product_updated = timezone.make_aware(datetime.strptime(magento_product['updated_at'], '%Y-%m-%d %H:%M:%S'), timezone=server_timezone)

        if product and (force or product.updated < magento_product_updated):
            product.magento_created_at = magento_product_created
            product.magento_updated_at = magento_product_updated
            product.name = magento_product['name']
            product.sku = magento_product['sku']
            product.price = Decimal(magento_product['price'])
            product.save()
            print('Product (ID {1.pk}){2} updated from Magento Product ID {0[id]}'.format(
                magento_product, product, ' force' if force else ''))
        elif not product:
            product = Product.objects.create(
                magento_id=magento_product['id'],
                magento_created_at=magento_product_created,
                magento_updated_at=magento_product_updated,
                name=magento_product['name'],
                sku=magento_product['sku'],
                price=Decimal(magento_product['price']))
            print('Product (ID {1.pk}) added for Magento Product ID {0[id]}'.format(magento_product, product))
        else:
            print('Skip Magento Product ID {0[id]}'.format(magento_product))
