import requests
from django.core.management import CommandError


def rest_url(host, rest_uri):
    return 'http://{0}/rest/V1/{1}'.format(host, rest_uri)


def auth(host, username, password):
    r = requests.post(rest_url(host, 'integration/admin/token'), json={
        'username': username,
        'password': password,
    })
    if r.status_code is not 200:
        raise CommandError('Cannot get access token. Status code: {0.status_code}. Response: {0.content}'.format(r))
    else:
        token = r.json()
        print('Access token is obtained')
    return token


def search_criteria(params):
    search_criteria = ''
    if len(params) > 0:
        for criteria in params:
            search_criteria += \
                'searchCriteria[filter_groups][0][filters][0][field]={}&' \
                'searchCriteria[filter_groups][0][filters][0][value]={}&' \
                'searchCriteria[filter_groups][0][filters][0][condition_type]={}'.format(*criteria)
    else:
        search_criteria = 'searchCriteria'
    return search_criteria
