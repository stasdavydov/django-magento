# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-01 08:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mymagento', '0002_auto_20160501_1515'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ('created', 'updated'), 'verbose_name': 'Product', 'verbose_name_plural': 'Products'},
        ),
        migrations.AlterField(
            model_name='product',
            name='magento_id',
            field=models.PositiveIntegerField(db_index=True, unique=True, verbose_name='Magento ID'),
        ),
    ]
