from __future__ import unicode_literals

from django.utils import timezone
from django.utils.translation import ugettext as _
from django.db import models


class AuditMixin(models.Model):
    created = models.DateTimeField(default=timezone.now, db_index=True, editable=False, verbose_name=_('Created'))
    updated = models.DateTimeField(default=timezone.now, db_index=True, editable=False, verbose_name=_('Updated'))

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        now = timezone.now()
        if not self.pk or not self.created:
            self.created = now
        self.updated = now
        super(AuditMixin, self).save(force_insert, force_update, using, update_fields)


class MagentoEntityMixin(models.Model):
    magento_id = models.PositiveIntegerField(unique=True, db_index=True, verbose_name=_('Magento ID'))
    magento_created_at = models.DateTimeField(db_index=True, verbose_name=_('Magento created at'))
    magento_updated_at = models.DateTimeField(db_index=True, verbose_name=_('Magento updated at'))

    class Meta:
        abstract = True


class Product(MagentoEntityMixin, AuditMixin):
    name = models.CharField(max_length=255, db_index=True, verbose_name=_('Name'))
    sku = models.CharField(max_length=255, db_index=True, verbose_name=_('SKU'))
    price = models.DecimalField(max_digits=8, db_index=True, decimal_places=2, verbose_name=_('Price'))

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')
        ordering = ('created', 'updated')

